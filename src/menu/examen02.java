
package menu;

/**
 *
 * @author Black
 */
public class examen02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        examen01 examen02 = new examen01();
        examen02.setNumRe(102);
        examen02.setNom("jose lopez");
        examen02.setDom("av del sol 1200");
        examen02.setTipoSer(1);
        examen02.setCostoPor(2.00f);
        examen02.setKillCon(450.00f);
        examen02.setFecha("21 marzo 2019");
         
        System.out.println("Num. Recibo: "+ examen02.getNumRe());
        System.out.println("Nombre: "+ examen02.getNom());
        System.out.println("Domicilio: "+ examen02.getDom());
        System.out.println("Tipo de servicio: "+ examen02.getTipoSer()+(": domicilio"));
        System.out.println("Costo por killowat: "+ examen02.getCostoPor());
        System.out.println("Killowats consumidos: " + examen02.getKillCon());
        System.out.println("Fecha: "+ examen02.getFecha());
        System.out.println("Calculo de pago subtotal:"+ examen02.calcularSubtotal());
        System.out.println("calculo de pago con impuesto:"+ examen02.calcularImpuesto());
        System.out.println("calculo de pago total:"+ examen02.calcularTotal());
        
        
        System.out.println("------------------------------------------------------------------------");
        
        //ejemplo 2
        examen02.setNumRe(103);
        examen02.setNom("Abarrotes feliz");
        examen02.setDom("av del sol");
        examen02.setTipoSer(2);
        examen02.setCostoPor(3.00f);
        examen02.setKillCon(1200);
        examen02.setFecha("21 marzo 2019");
        
        System.out.println("Num. Recibo: "+ examen02.getNumRe());
        System.out.println("Nombre: "+ examen02.getNom());
        System.out.println("Domicilio: "+ examen02.getDom());
        System.out.println("Tipo de servicio: "+ examen02.getTipoSer()+(": domicilio"));
        System.out.println("Costo por killowat: "+ examen02.getCostoPor());
        System.out.println("Killowats consumidos: " + examen02.getKillCon());
        System.out.println("Fecha: "+ examen02.getFecha());
        System.out.println("Calculo de pago subtotal:"+ examen02.calcularSubtotal());
        System.out.println("calculo de pago con impuesto:"+ examen02.calcularImpuesto());
        System.out.println("calculo de pago total:"+ examen02.calcularTotal());

    }
    
}
