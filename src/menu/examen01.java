
package menu;

/**
 *
 * @author Black
 */
public class examen01 {
    
    private int id;
    private int numRe;
    private String fecha;
    private String nom;
    private String dom;
    private int tipoSer;
    private float costoPor;
    private float killCon;
    private int status;
  
    
    public examen01(){
    this.id=0;
    this.numRe=0;
    this.nom="";
    this.dom="";
    this.tipoSer=0;
    this.costoPor=0.0f;
    this.fecha="";
    this.killCon=0.0f;
    this.status=0;
    
    
    }
    //Constructor por argumentos
    public examen01(int id, int numRe, String nom, String dom, int tipoSer, float costoPor, String fecha, int killCon, int status){
    this.id=id;
    this.numRe=numRe;
    this.nom=nom;
    this.dom=dom;
    this.tipoSer=tipoSer;
    this.costoPor=costoPor;
    this.fecha=fecha;
    this.killCon=killCon;
    this.status=status;
   
    }
    
    //Copia "OTRO"
    public examen01(examen01 otro){
    this.id=otro.id;
    this.numRe=otro.numRe;
    this.nom=otro.nom;
    this.dom=otro.dom;
    this.tipoSer=otro.tipoSer;
    this.costoPor=otro.costoPor;
    this.fecha=otro.fecha;
    this.killCon=otro.killCon;
    this.status=otro.status;
    }
    

    //Metodos Set y Get
     public int getId() {    
        return id;
    }

    public int getStatus() {
        return status;
    }
   
    public int getNumRe() {
        return numRe;
    }

    public void setNumRe(int numRe) {
        this.numRe = numRe;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDom() {
        return dom;
    }

       public void setId(int id) {
        this.id = id;
    }
    
     public void setStatus(int status) {
        this.status = status;
    }

    
    public void setDom(String dom) {
        this.dom = dom;
    }

    public int getTipoSer() {
        return tipoSer;
    }

    public void setTipoSer(int tipoSer) {
        this.tipoSer = tipoSer;
    }

    public float getCostoPor() {
        return costoPor;
    }

    public void setCostoPor(float costoPor) {
        this.costoPor = costoPor;
    }

    public float getKillCon() {
        return killCon;
    }

    public void setKillCon(float killCon) {
        this.killCon = killCon;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    
    

    
    //Metodos de comportamiento

     public float calcularSubtotal(){
    float subtotal=0.0f;
    if (this.tipoSer == 1) {
        subtotal=(this.killCon*this.costoPor);
    }
    if (this.tipoSer == 2) {
        subtotal=(this.killCon*this.costoPor);
    }
    if (this.tipoSer == 3) {
         subtotal=(this.killCon*this.costoPor);
    }
    return subtotal;
    }
    
    public float calcularImpuesto(){
    float impuesto=0.0f;
    impuesto=this.calcularSubtotal()*0.16f;
    return impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularSubtotal()+this.calcularImpuesto();
    return total;
    }
    
}

