package menu;

/**
 *
 * @author Black
 */
import java.sql.*;
public class dbRecibo {
    
    
    private String MYSQLDRIVER="com.mysql.cj.jdbc.Driver";
    private String MYSQLDB="jdbc:mysql://3.132.136.208:3306/edgar?user=edgar&password=cacalotan";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registros;
    
    //constructor
    public dbRecibo(){
        
        try {
            Class.forName(MYSQLDRIVER);
            
        } catch( ClassNotFoundException e){
        
            System.out.println("Surgio un ERROR"+ e.getMessage());
            System.exit(-1);
            
        }
        
    }
       public void conectar(){
            
        try {
        conexion = DriverManager.getConnection(MYSQLDB);
        } catch(SQLException e){
            
            System.out.println("No se logró conectar "+e.getMessage());
        }
        
        }
       
       public void desconectar(){
       
       try {
           
       conexion.close();
       
       } catch(SQLException e){
       
       System.out.println("Surgió un error al desconectar "+ e.getMessage());
       }
    
}
       
       public void insertar(examen01 rec){
           conectar();
           try{
               
       strConsulta= "INSERT INTO Recibo(numRe, fecha, nom, dom, tipoSer, costoPor, killCon, status) "
               + "VALUES(?,CURDATE(),?,?,?,?,?,?)";
       
            PreparedStatement pst = conexion.prepareStatement(strConsulta);       
            pst.setInt(1, rec.getNumRe());
            pst.setString(2, rec.getNom());
            pst.setString(3, rec.getDom());
            pst.setInt(4, rec.getTipoSer());
            pst.setFloat(5, rec.getCostoPor());
            pst.setFloat(6, rec.getKillCon());
            pst.setInt(7, rec.getStatus());
            
            pst.executeUpdate();
           } catch (SQLException e) {
            System.out.println("Error al insertar "+ e.getMessage());
        }
           desconectar();
}
         public void actualizar(examen01 rec){
             examen01 examen02 = new examen01();
          
               
       strConsulta= "UPDATE Recibo SET nom = ?,dom = ?,fecha = ?,tipoSer = " +
               "? ,costoPor = ?,killCon = ? WHERE numRe = ? and status =0;";
        this.conectar();
           try{
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setString(1, rec.getNom());
            pst.setString(2, rec.getDom());
            pst.setString(3, rec.getFecha());
            pst.setInt(4, rec.getTipoSer());
            pst.setFloat(5, rec.getCostoPor());
            pst.setFloat(6, rec.getKillCon());
            pst.setInt(7, rec.getNumRe());
            
            pst.executeUpdate();
            this.desconectar();
           } 
           catch (SQLException e) {
            System.out.println("surgio un error al actualizar "+ e.getMessage());
           }
         }
         
         public void habilitar(examen01 rec){
             
             String consulta = "";
             strConsulta = "UPDATE Recibo SET status = 0 WHERE numRe = ?";
             this.conectar();
             try {
                 System.err.println("se conecto");
                 PreparedStatement pst = conexion.prepareStatement(strConsulta);
                 // asignar los valores a la consulta
                 pst.setInt(1, rec.getNumRe());
                 
                 pst.executeUpdate();
                 this.desconectar();
             } catch (SQLException e) {
                 System.err.println("surgio un error al habilitar" + e.getMessage());
             }
         }
         
             public void deshabilitar(examen01 rec){
             strConsulta = "UPDATE Recibo SET status = 1 WHERE numRe = ?";
             this.conectar();
             try {
                 System.err.println("se conecto");
                 PreparedStatement pst = conexion.prepareStatement(strConsulta);
                 // asignar los valores a la consulta
                 pst.setInt(1, rec.getNumRe());
                 
                 pst.executeUpdate();
                 this.desconectar();
             } catch (SQLException e) {
                 System.err.println("surgio un error al habilitar" + e.getMessage());
             }
         
        }
             
             public boolean isExiste(int numRe, int status) {
                 boolean exito = false;
                 this.conectar();
                 strConsulta = "SELECT * FROM Recibo WHERE numRe = ? and status = ?;";
                 try {
                     PreparedStatement pst = conexion.prepareStatement(strConsulta);
                     pst.setInt(1, numRe);
                     pst.setInt(2, status);
                     this.registros = pst.executeQuery();
                     if(this.registros.next()) exito = true;
                 } catch(SQLException e){
                 System.err.println("surgio un error al verificar si existe: " + e.getMessage());
                 
             }
                 this.desconectar();
                 return exito;
}
             public examen01 buscar(int numRe){
                 examen01 examen02 = new examen01();
                 conectar();
                 try {
                     strConsulta = "SELECT * FROM Recibo WHERE numRe = ? and status = 0;";
                     PreparedStatement pst = conexion.prepareStatement(strConsulta);
                     
                     pst.setInt(1, numRe);
                     this.registros = pst.executeQuery();
                     if(this.registros.next()){
                         
                         examen02.setId(registros.getInt("id"));
                         examen02.setNumRe(registros.getInt("numRe"));
                         examen02.setNom(registros.getString("nom"));
                         examen02.setDom(registros.getString("dom"));
                         examen02.setTipoSer(registros.getInt("tipoSer"));
                         examen02.setCostoPor(registros.getFloat("costoPor"));
                         examen02.setKillCon(registros.getFloat("killCon"));
                         examen02.setFecha(registros.getString("fecha"));
                         
                         
                     } else examen02.setId(0);
                 }
                 catch(SQLException e){
                     System.err.println("surgio un error al habilitar: " + e.getMessage());
                     
                 }
                 this.desconectar();
                 return examen02;
             }
}

